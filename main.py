from fastapi import FastAPI
from spellchecker import SpellChecker
from textblob import TextBlob
from deep_translator import GoogleTranslator

app = FastAPI()

@app.get("/")
def home():
    return {"Mensaje": "SpellChecker"}

@app.get("/checker/{palabraRecibida}")
async def read_item(palabraRecibida):
    if palabraRecibida is None:
        return "No se recibio una respuesta"
    else:
        spell = SpellChecker(language='es')

        # find those words that may be misspelled
        misspelled = spell.unknown([palabraRecibida])

        for word in misspelled:
            # Get the one `most likely` answer
            print(spell.correction(word))

            palabra = spell.candidates(word)
            # Get a list of `likely` options
            return {"candidatos": palabra}



@app.get("/sentenceChecker/{oracionRecibida}")
async def read_item(oracionRecibida):
    #texto que ingresa para corrección
    texto = oracionRecibida
    translated = GoogleTranslator(source='auto', target='en').translate(texto)
    sentence = TextBlob(translated)
    correct = sentence.correct()
    backed = GoogleTranslator(source='auto', target='es').translate(str(correct))
    print("Texto inicial con errores: " + texto + "\n", "Texto corregido: " + backed + "\n", "Sentimiento: " + str(sentence.sentiment))
    return {"textoCorregido": backed}


